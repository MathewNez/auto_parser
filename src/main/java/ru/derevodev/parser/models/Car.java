package ru.derevodev.parser.models;

public class Car {
    String name;
    String year;
    String imageUrl;
    String specialInfo;
    String price;
    String adLink;
    String geo;
    String engineCapacity;
    String gearbox;
    String horsepower;
    String mileage;
    String bodyType;
    String driveUnit;
    String fuel;
    String color;

    public String getAdLink() {
        return adLink;
    }

    public String getBodyType() {
        return bodyType;
    }

    public String getColor() {
        return color;
    }

    public String getDriveUnit() {
        return driveUnit;
    }

    public String getEngineCapacity() {
        return engineCapacity;
    }

    public String getFuel() {
        return fuel;
    }

    public String getGearbox() {
        return gearbox;
    }

    public String getGeo() {
        return geo;
    }

    public String getHorsepower() {
        return horsepower;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getMileage() {
        return mileage;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getSpecialInfo() {
        return specialInfo;
    }

    public String getYear() {
        return year;
    }

    public void setAdLink(String adLink) {
        this.adLink = adLink;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setDriveUnit(String driveUnit) {
        this.driveUnit = driveUnit;
    }

    public void setEngineCapacity(String engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public void setHorsepower(String horsepower) {
        this.horsepower = horsepower;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setSpecialInfo(String specialInfo) {
        this.specialInfo = specialInfo;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return String.format("{name: %s, year: %s, image_url: %s, special_info: %s, price: %s, " +
                              "ad_link: %s, geo: %s, engine_capacity: %s, gearbox: %s, " +
                              "horsepower: %s, mileage: %s, body_type: %s, drive_unit: %s, " +
                              "fuel: %s}",
                              this.name, this.year, this.imageUrl, this.specialInfo, this.price,
                              this.adLink, this.geo, this.engineCapacity, this.gearbox,
                              this.horsepower, this.mileage, this.bodyType, this.driveUnit,
                              this.fuel);
    }
}
