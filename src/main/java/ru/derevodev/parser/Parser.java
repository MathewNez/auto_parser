package ru.derevodev.parser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import ru.derevodev.parser.models.Car;
import ru.derevodev.parser.parsers.AvitoParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class Parser {

    public static void main(String[] args) throws IOException {
        String driverPath = getDriverPath();
        System.setProperty("webdriver.gecko.driver", driverPath);
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.avito.ru/ekaterinburg/avtomobili?cd=1&radius=200&s=104");
        String pageSource = driver.getPageSource();
        driver.quit();
        List<Car> parsedCars = AvitoParser.parseCars(pageSource);
        for (Car car : parsedCars) {
            System.out.println(car.toString());
        }
    }


    private static String getDriverPath() throws IOException {
        InputStream driverPath = Parser.class.getClassLoader().getResourceAsStream("conf.properties");
        Properties prop = new Properties();
        prop.load(driverPath);
        return prop.getProperty("geckodriver");
    }
}
