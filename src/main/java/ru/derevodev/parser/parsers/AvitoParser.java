package ru.derevodev.parser.parsers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.derevodev.parser.models.Car;

import java.util.*;

public class AvitoParser {

    public static String getImageURL(Element car) {
        String url;
        try {
            url = Objects.requireNonNull(car.select("img[itemprop=image]").first()).attr("src");
        }
        catch (NullPointerException ignored){
            try {
                url = Objects.requireNonNull(
                        car.select("li[class^=photo-]").first()).attr("data-marker"
                );
            }
            catch (NullPointerException ignored2) {
                return null;
            }
            url = url.replace("slider-image/image-", "");
        }
        return url;
    }

    public static String[] getNameNYear(Element car) {
        String title;
        try{
            title = Objects.requireNonNull(car.select("h3[itemprop=name]").first()).text();
        }
        catch (NullPointerException ignored){
            return new String[]{null, null};
        }
        String[] nameNYear = title.split(", ");
        if (nameNYear.length < 2)
            return new String[]{title, null};
        return nameNYear;
    }

    public static List<String> getParams(Element car) {
        String params;
        try {
            params = Objects.requireNonNull(
                    car.select("div[data-marker=item-specific-params]").first()).text();
        }
        catch(NullPointerException ignored) {
            return null;

        }

        String[] paramsArr = params.split(", ");
        String specialInfo = null;
        List<String> allParams;
        try {
            if (!paramsArr[0].contains("км")) {
                if (paramsArr[0].contains("л.с.")) {
                    String[] zeroMileage = new String[]{"0 км. (новый)"};
                    String[] paramsExtended = Arrays.copyOf(zeroMileage, paramsArr.length + zeroMileage.length);
                    System.arraycopy(zeroMileage, 0, paramsExtended, 1, paramsExtended.length);
                    paramsArr = paramsExtended;
                }
                else {
                    specialInfo = paramsArr[0];
                    paramsArr = Arrays.copyOfRange(paramsArr, 1, paramsArr.length);
                }
            }
            paramsArr = Arrays.copyOf(paramsArr, paramsArr.length+1);
            paramsArr[paramsArr.length - 1] = specialInfo;
            String[] mainParams = paramsArr[1].split(" ");
            allParams = new ArrayList<>(paramsArr.length + mainParams.length);
            Collections.addAll(allParams, paramsArr);
            Collections.addAll(allParams, mainParams);
        } catch (ArrayIndexOutOfBoundsException ignored) {
            return new ArrayList<>();
        }

        return allParams;
    }

    public static String getLink(Element car) {
        String linkEnd;
        try {
            linkEnd = Objects.requireNonNull(
                    car.select("a[data-marker=item-title]").first()).attr("href");
        }
        catch (NullPointerException ignored){
            return null;
        }

        return String.format("https://avito.ru%s", linkEnd);
    }

    public static Car parseCar(Element car) {
        Car parsedCar = new Car();
        parsedCar.setImageUrl(getImageURL(car));
        String[] nameNYear = getNameNYear(car);
        try {
            parsedCar.setName(nameNYear[0]);
            parsedCar.setYear(nameNYear[1]);
        } catch (ArrayIndexOutOfBoundsException ignored) {
            parsedCar.setName(nameNYear[0]);
        }
        List<String> carParams = getParams(car);
        try {
            assert carParams != null;
            parsedCar.setMileage(carParams.get(0));
            parsedCar.setBodyType(carParams.get(2));
            parsedCar.setDriveUnit(carParams.get(3));
            parsedCar.setFuel(carParams.get(4));
            parsedCar.setSpecialInfo(carParams.get(5));
            parsedCar.setEngineCapacity(carParams.get(6));
            parsedCar.setGearbox(carParams.get(7));
            parsedCar.setHorsepower(carParams.get(8));
        } catch (IndexOutOfBoundsException ignored) {}
        try{
            parsedCar.setGeo(Objects.requireNonNull(car.select("div[class^=geo-]").first()).text());
            parsedCar.setPrice(Objects.requireNonNull(car.select("span[data-marker=item-price]").first()).text());
        }
        catch (NullPointerException ignored) {}
        parsedCar.setAdLink(getLink(car));

        return parsedCar;
    }

    public static Elements getCars(String pageSource) {
        Document doc = Jsoup.parse(pageSource);

        return doc.select("div[data-marker=item]");
    }

    public static List<Car> parseCars(String pageSource) {
        List<Car> parsedCars = new ArrayList<>();
        Elements cars = getCars(pageSource);
        for (Element car : cars) {
            parsedCars.add(parseCar(car));
        }

        return parsedCars;
    }


}
