# Auto_parser

Auto_parser is a project that scaps avito.ru for new cars.

## Developing

1. Clone project
```shell
git clone <url>
```

2. Go to project root folder
```shell
cd auto_parser
```

3. Install dependencies
```shell
mvn dependency:resolve
```

4. Create `conf.properties` at `src/resources`
```shell
cd src/
mkdir resources
cd resources/
touch conf.properties
```

5. In `conf.properties`, specify path to binafy file of geckodriver this way
```.properties
geckodriver=/usr/local/bin/geckodriver
```
 Now you are ready to run the project.